# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [0.2.1] - 2025-01-12

### Fixed
- Mocking Protobuf messages are now stored in the right directory.
- Mocking binary and data are now installed in the right directory.

## [0.2.0] - 2025-01-11

### Added
- Unittests for CI/CD.
- Mocking SSC DSP with ssc-server to emulate DSP on non-Qualcomm environments.
- Release instructions in RELEASE.md.
- Set `G_LOG_DOMAIN` during build to 'libssc' for easier debugging.
- Wait for DSP to become available during boot.

### Changed
- Reduced API surface. All private interfaces are now excluded from public usage.
- API is now solely exposed through `libssc.h`.
- Use proper GErrors instead of g\_warning for errors.

### Fixed
- Passing GTask in libssc-sensor was missing.
- Light sensor intensity is always positive, reject negative measurements.
- Proximity sensor will always output a measurement now when opened.
- Document availability sensor attribute and specify QMI header length.
- Path to protoc and protoc-c for compiling ProtoBuff messages.
- Public headers are now correctly installed.
- Sync API race conditions.

## [0.1.6] - 2024-06-08

### Fixed
- Ownership of GError is now properly performed when using GTasks.
- Handle access denied to address family AF\_QIPCRTR in lockdown mode.
- Fix verbose argument for enabling debug mode in ssccli.
- Race condition if the sensor is rapidly probed by opening and closing it.

## [0.1.5] - 2024-05-17

### Fixed
- Fixed LICENSE to properly indicate GPLv3.
- Add architecture diagram referenced in reverse engineering notes.
- Fix .so library name to avoid twice 'lib': liblibssc.so

### Added
- Timeout parameter for ssccli.
- Flush stdout after each sensor measurement in ssccli.
- Add support for proximity sensor of xiaomi-davinci (SM7150).

### Changed
- Expanded README with proper description and link to landing page.
- Added build instructions to README.
- Added custom domain for landing page in README.

## [0.1.4] - 2023-08-19

### Fixed
- Link with math library.
- Assert QMI client not NULL during sensor disposing.

### Added
- Define unknown type in QMI Report messages as Report Type.

### Changed
- Update to latest libqmi API changes.
- Search for sensors with a non-zero sample rate such as sensors on Pixel 3A.

## [0.1.3] - 2023-05-20

### Fixed
- Release QMI client before sensor disposing.

## [0.1.2] - 2023-05-20

### Fixed
- Segfault with unknown parameter.
- Segfault during sensor disposing.

## [0.1.1] - 2023-05-01

### Fixed
- Fix format-security warning.

## [0.1.0] - 2023-05-01

Initial release.

<!-- links to diffs between releases -->
[0.1.0]: https://codeberg.org/DylanVanAssche/libssc/compare/01fe59f06aa107a556dd2cdf33e65fd6378eaf32...0.1.0
[0.1.1]: https://codeberg.org/DylanVanAssche/libssc/compare/0.1.0...0.1.1
[0.1.2]: https://codeberg.org/DylanVanAssche/libssc/compare/0.1.1...0.1.2
[0.1.3]: https://codeberg.org/DylanVanAssche/libssc/compare/0.1.2...0.1.3
[0.1.4]: https://codeberg.org/DylanVanAssche/libssc/compare/0.1.3...0.1.4
[0.1.5]: https://codeberg.org/DylanVanAssche/libssc/compare/0.1.4...0.1.5
[0.1.6]: https://codeberg.org/DylanVanAssche/libssc/compare/0.1.5...0.1.6
[0.2.0]: https://codeberg.org/DylanVanAssche/libssc/compare/0.1.6...v0.2.0
[0.2.1]: https://codeberg.org/DylanVanAssche/libssc/compare/v0.2.0...v0.2.1
